const request = require("supertest")
const app = require("../app")


describe('GET /profile', () => {
    test('should return 200 OK', async () => {
        const response = await request(app).get('/api/v1/rank');
        expect(response.statusCode).toBe(200);
    });
});
